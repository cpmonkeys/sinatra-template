# frozen_string_literal: true

def tailwindcss_base_command
  [
    'bin/tailwindcss',
    '-i', 'app/assets/stylesheets/application.tailwind.css',
    '-o', 'app/assets/builds/tailwind.css',
    '-c', 'config/tailwind.config.js'
  ]
end

desc 'spawn a new console'
task :console do
  ruby 'bin/console.rb'
end

namespace :htmx do
  desc 'download htmx'
  task :download, [:version] do |_t, args|
    version = args[:version] || '1.9.10'
    sh "curl -L -o app/assets/builds/htmx.min.js https://unpkg.com/htmx.org@#{version}/dist/htmx.min.js"
  end
end

namespace :tailwindcss do
  desc 'download the lastest version of tailwinds binary'
  task :download, [:version] do |_t, args|
    version = args[:version] || 'v3.4.1'
    sh 'mkdir -p bin'
    platform = Gem::Platform.local
    os = 'macos' if platform.os == 'darwin'
    sh "curl -L -o bin/tailwindcss https://github.com/tailwindlabs/tailwindcss/releases/download/#{version}/tailwindcss-#{os}-#{platform.cpu}"
    sh "chmod +x bin/tailwindcss"
  end

  desc 'run tailwindcss in watch mode'
  task :watch do
    command = tailwindcss_base_command << '-w'
    system(*command)
  end

  desc 'build css'
  task :build do
    command = tailwindcss_base_command << '-m'
    system(*command)
  end
end

desc 'run a dev server'
task :server do
  command = [
    'bundle', 'exec', 'rackup', 'config/config.ru', '-p', '3000'
  ]
  system(*command)
end

namespace :db do
  desc 'run migrations'
  task :migrate, [:version] do |_t, args|
    require 'sequel/core'
    Sequel.extension :migration
    version = args[:version].to_i if args[:version]
    Sequel.connect(ENV.fetch('POSTGRES_URL')) do |db|
      Sequel::Migrator.run(db, 'db/migrations', target: version)
    end
  end
end
