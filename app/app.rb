# frozen_string_literal: true

require 'sinatra'

module Routes
  # a few helpers available in all views
  module Helpers
    def to_kebab(str)
      str.to_s.tr('_', '-')
    end

    def html_attributes(attrs)
      attrs.each_pair.map { |k, v| "#{k}=\"#{v}\"" }.join(' ')
    end

    def _render(view, layout: true)
      is_htmx = request.env['HTTP_HX_REQUEST'] == 'true'
      erb view.to_sym, layout: (is_htmx ? false : layout)
    end

    def _component(view, **attrs, &block)
      @attrs = attrs.transform_keys { |k| to_kebab(k) }
      erb "components/#{view}".to_sym, layout: false, &block
    end
  end

  # root routes
  class Application < Sinatra::Base
    set :public_folder, __dir__ + '/assets/builds'
    set :views, __dir__ + '/views'
    helpers Helpers

    get '/' do
      _render :home
    end

    get '/foo' do
      _render :foo
    end
  end
end
