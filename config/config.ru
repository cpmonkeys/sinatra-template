# frozen_string_literal: true

require 'sinatra'
require_relative '../app/app'

routes = {
  '/' => Routes::Application
}

run Rack::URLMap.new(routes)
